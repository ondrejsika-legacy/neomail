# python
from email import message_from_string

# django
from django.db import models
from django.db.models.signals import post_save

# project
from mail.models import Mail
from mail.utils import sendmail


class ParsedMail(models.Model):
    mail = models.OneToOneField(Mail)

    subject = models.CharField(max_length=255, null=True, blank=True)
    mail_from = models.CharField(max_length=255, null=True, blank=True)
    mail_to = models.CharField(max_length=255, null=True, blank=True)
    reply_to = models.CharField(max_length=255, null=True, blank=True)
    cc = models.CharField(max_length=255, null=True, blank=True)
    content_type = models.CharField(max_length=255, null=True, blank=True)
    message_id = models.CharField(max_length=255, null=True, blank=True)
    body = models.TextField(null=True, blank=True)

    @staticmethod
    def parse_mail(mail_id, data):
        msg = message_from_string(data)
        parsed_message = ParsedMail(mail_id=mail_id)
        parsed_message.subject = msg['subject']
        parsed_message.mail_from = msg['from']
        parsed_message.mail_to = msg['to']
        parsed_message.reply_to = msg['reply-to']
        parsed_message.cc = msg['cc']
        parsed_message.content_type = msg['content-type']
        parsed_message.message_id = msg['message-id']
        parsed_message.body = data.split('\n\n', 1)[1] if msg.keys() else data
        parsed_message.save()
        return parsed_message

    def __unicode__(self):
        return u'#%s parsed' % self.id


class SendMail(models.Model):
    subject = models.CharField(max_length=255, null=True, blank=True)
    mail_from = models.CharField(max_length=255, null=True, blank=True)
    mail_to = models.CharField(max_length=255, null=True, blank=True)
    reply_to = models.CharField(max_length=255, null=True, blank=True)
    cc = models.CharField(max_length=255, null=True, blank=True)
    content_type = models.CharField(max_length=255, null=True, blank=True)
    body = models.TextField(null=True, blank=True)

    is_send = models.BooleanField(default=False)

    def send(self):
        self.is_send = sendmail(mail_from=self.mail_from,
                                mail_to_list=self.mail_to,
                                subject=self.subject,
                                body=self.body, )
        print self.is_send
        self.save()


# Signals

def mail_post_save_action(sender, instance, **kwargs):
     ParsedMail.parse_mail(instance.id, instance.data)


post_save.connect(mail_post_save_action, Mail)