# django
from django.conf.urls import patterns, include, url


urlpatterns = patterns('client.views',
    url(r'^$', 'inbox', name='inbox'),
    url(r'^(?P<account_id>\d+)/$', 'inbox', name='inbox'),
    url(r'^mail/(?P<mail_id>\d+)/$', 'mail_detail', name='mail_detail'),
    url(r'^send-mail/$', 'send_mail', name='send_mail'),
)
