# django
from django import forms

# local
from .models import SendMail


class SendMailForm(forms.ModelForm):
    class Meta:
        model = SendMail
        exclude = ('is_send', )
