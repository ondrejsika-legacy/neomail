# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'ParsedEmail'
        db.create_table(u'client_parsedemail', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('mail', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['mail.Mail'], unique=True)),
            ('subject', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('mail_from', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('mail_to', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('reply_to', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('cc', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('content_type', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('message_id', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('body', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'client', ['ParsedEmail'])


    def backwards(self, orm):
        # Deleting model 'ParsedEmail'
        db.delete_table(u'client_parsedemail')


    models = {
        u'client.parsedemail': {
            'Meta': {'object_name': 'ParsedEmail'},
            'body': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'cc': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'content_type': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mail': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['mail.Mail']", 'unique': 'True'}),
            'mail_from': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'mail_to': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'message_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'reply_to': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'subject': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'mail.mail': {
            'Meta': {'ordering': "('-id',)", 'object_name': 'Mail'},
            'data': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mail_from': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'mail_to': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        }
    }

    complete_apps = ['client']