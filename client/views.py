# django
from django.shortcuts import render, get_object_or_404, HttpResponseRedirect

# django contrib
from django.contrib.auth.decorators import login_required

# project
from mail.models import Account, Mail, BoxMail, Box

# local
from .forms import SendMailForm


@login_required
def inbox(request, account_id=None):
    accounts = Account.objects.filter(user=request.user)
    if account_id:
        account = get_object_or_404(Account, id=account_id, user=request.user)
        mails = Mail.objects.filter(boxmail__box__account=account).distinct()
    else:
        mails = Mail.objects.filter(boxmail__box__account__user=request.user).distinct()

    return render(request, 'client/inbox.html', {
        'accounts': accounts,
        'mails': mails,
    })


@login_required
def mail_detail(request, mail_id):
    accounts = Account.objects.filter(user=request.user)
    mail = get_object_or_404(Mail, id=mail_id)

    return render(request, 'client/mail_detail.html', {
        'accounts': accounts,
        'mail': mail,
    })


@login_required
def send_mail(request):
    accounts = Account.objects.filter(user=request.user)
    form = SendMailForm(request.POST or None)
    if form.is_valid():
        obj = form.save()
        obj.send()
        return HttpResponseRedirect('?msg=sent')

    return render(request, 'client/send_mail.html', {
        'accounts': accounts,
        'form': form,
    })
