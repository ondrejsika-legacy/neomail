# django
from django.contrib import admin

# local
from .models import ParsedMail, SendMail


admin.site.register(ParsedMail)
admin.site.register(SendMail)
