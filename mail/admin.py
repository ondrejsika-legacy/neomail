# django
from django.contrib import admin

# local
from .models import Account, Mail, Box, BoxMail, Forward


admin.site.register(Account)
admin.site.register(Box)
admin.site.register(BoxMail)
admin.site.register(Mail)
admin.site.register(Forward)