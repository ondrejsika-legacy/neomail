# python
import re

# local
from models import Box, Mail, Account, BoxMail, Forward
from utils import parse_email_address, get_strict_address, forward_mail


def process_message(mail_from, mail_to_list, data):
    mail_from = get_strict_address(mail_from)
    for mail_to in mail_to_list:
        mail_to = get_strict_address(mail_to)

        username, box, domain = parse_email_address(mail_to)
        account, created = Account.objects.get_or_create(username=username,
                                                         box=box,
                                                         domain=domain)
        if created:
            if box:
                try:
                    account.user = Account.objects.get(username=username,
                                                       domain=domain,
                                                       box='').user
                except Account.DoesNotExist:
                    pass
            account.save()

        primary_box, created = Box.objects.get_or_create(account=account,
                                                         path='INBOX')
        if created:
            primary_box.save()

        if box:
            secondary_box, created = Box.objects.get_or_create(account=account,
                                                               path='INBOX/%s' % box)
            if created:
                secondary_box.save()

        mail = Mail(mail_to=mail_to, mail_from=mail_from, data=data)
        mail.save()

        BoxMail(box=primary_box, mail=mail).save()
        if box:
            BoxMail(box=secondary_box, mail=mail).save()

        # ########## FORWARDING ##########

        forward_accounts = Account.objects.filter(username__in=['*', username], domain=domain)
        forwards = Forward.objects.filter(account__in=forward_accounts)
        for forward in forwards:
            if re.match(forward.email_re, mail_to):
                forward_mail(mail_from, forward.forward_to_email, data)

