# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Mail'
        db.create_table(u'mail_mail', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('mail_from', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('mail_to', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('data', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'mail', ['Mail'])

        # Adding model 'Account'
        db.create_table(u'mail_account', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'], null=True, blank=True)),
            ('username', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('box', self.gf('django.db.models.fields.CharField')(default='', max_length=64, blank=True)),
            ('domain', self.gf('django.db.models.fields.CharField')(max_length=64)),
        ))
        db.send_create_signal(u'mail', ['Account'])

        # Adding unique constraint on 'Account', fields ['username', 'box', 'domain']
        db.create_unique(u'mail_account', ['username', 'box', 'domain'])

        # Adding model 'Box'
        db.create_table(u'mail_box', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('account', self.gf('django.db.models.fields.related.ForeignKey')(related_name='box_set', to=orm['mail.Account'])),
            ('path', self.gf('django.db.models.fields.CharField')(max_length=64)),
        ))
        db.send_create_signal(u'mail', ['Box'])

        # Adding unique constraint on 'Box', fields ['account', 'path']
        db.create_unique(u'mail_box', ['account_id', 'path'])

        # Adding model 'BoxMail'
        db.create_table(u'mail_boxmail', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('box', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mail.Box'])),
            ('mail', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mail.Mail'])),
        ))
        db.send_create_signal(u'mail', ['BoxMail'])


    def backwards(self, orm):
        # Removing unique constraint on 'Box', fields ['account', 'path']
        db.delete_unique(u'mail_box', ['account_id', 'path'])

        # Removing unique constraint on 'Account', fields ['username', 'box', 'domain']
        db.delete_unique(u'mail_account', ['username', 'box', 'domain'])

        # Deleting model 'Mail'
        db.delete_table(u'mail_mail')

        # Deleting model 'Account'
        db.delete_table(u'mail_account')

        # Deleting model 'Box'
        db.delete_table(u'mail_box')

        # Deleting model 'BoxMail'
        db.delete_table(u'mail_boxmail')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'mail.account': {
            'Meta': {'unique_together': "(('username', 'box', 'domain'),)", 'object_name': 'Account'},
            'box': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '64', 'blank': 'True'}),
            'domain': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']", 'null': 'True', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'mail.box': {
            'Meta': {'unique_together': "(('account', 'path'),)", 'object_name': 'Box'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'box_set'", 'to': u"orm['mail.Account']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'path': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'mail.boxmail': {
            'Meta': {'object_name': 'BoxMail'},
            'box': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mail.Box']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mail': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mail.Mail']"})
        },
        u'mail.mail': {
            'Meta': {'ordering': "('-id',)", 'object_name': 'Mail'},
            'data': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mail_from': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'mail_to': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        }
    }

    complete_apps = ['mail']