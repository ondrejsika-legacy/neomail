# python
import smtplib
from email.utils import parseaddr
from email.mime.text import MIMEText

# django
from django.conf import settings


OUTGOING_SMTP_SERVER_HOST = getattr(settings, 'OUTGOING_SMTP_SERVER_HOST', '127.0.0.1')
OUTGOING_SMTP_SERVER_PORT = getattr(settings, 'OUTGOING_SMTP_SERVER_PORT', 26)


def get_strict_address(s):
    return parseaddr(s.lower())[1]


def parse_email_address(email):
    user_part, domain = email.lower().split('@')
    if not '+' in user_part:
        return user_part, '', domain
    username, box = user_part.split('+', 1)
    return username, box, domain


def sendmail_raw(mail_from, mail_to_list, data, server=OUTGOING_SMTP_SERVER_HOST, port=OUTGOING_SMTP_SERVER_PORT):
    try:
        conn = smtplib.SMTP(server, port)
        conn.sendmail(mail_from, mail_to_list, data)
        conn.quit()
    except:
        return False
    return True


def sendmail(mail_from, mail_to_list, subject, body):
    msg = MIMEText(body)
    msg['From'] = mail_from
    msg['To'] = mail_to_list
    msg['Subject'] = subject
    return sendmail_raw(mail_from, mail_to_list.split(','), msg.as_string())


def forward_mail(mail_from, forward_to, data):
    # fixme use mail queue
    return sendmail_raw(mail_from, forward_to, data)
