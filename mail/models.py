# django
from django.db import models

# django contrib
from django.contrib.auth.models import User


class Mail(models.Model):
    mail_from = models.CharField(max_length=128)
    mail_to = models.CharField(max_length=128)
    data = models.TextField()

    class Meta:
        ordering = ('-id', )

    def __unicode__(self):
        return u'#%s' % self.id


class Account(models.Model):
    user = models.ForeignKey(User, null=True, blank=True)

    username = models.CharField(max_length=64)
    box = models.CharField(max_length=64, default='', blank=True)
    domain = models.CharField(max_length=64)

    class Meta:
        unique_together = (('username', 'box', 'domain'), )

    def __unicode__(self):
        if self.box:
            return '%s+%s@%s' % (self.username, self.box, self.domain)
        return '%s@%s' % (self.username, self.domain)


class Box(models.Model):
    account = models.ForeignKey(Account, related_name='box_set')
    path = models.CharField(max_length=64)

    class Meta:
        unique_together = (('account', 'path'), )

    def __unicode__(self):
        return u'%s %s' % (self.account, self.path)


class BoxMail(models.Model):
    box = models.ForeignKey(Box)
    mail = models.ForeignKey(Mail)


class Forward(models.Model):
    account = models.ForeignKey(Account)

    email_re = models.CharField(max_length=128)
    forward_to_email = models.CharField(max_length=128)

    def __unicode__(self):
        return u'%s -> %s (%s)' % (self.email_re, self.forward_to_email, self.account)
