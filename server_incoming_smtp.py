#!/usr/bin/python

# python
import os
import smtpd
import asyncore

# django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
from django.conf import settings

# project
from mail import process_message

INCOMING_SMTP_SERVER_HOST = getattr(settings, 'INCOMING_SMTP_SERVER_HOST', '0.0.0.0')
INCOMING_SMTP_SERVER_PORT = getattr(settings, 'INCOMING_SMTP_SERVER_PORT', 25)


class SMTPServer(smtpd.SMTPServer):
    def process_message(self, peer, mail_from, mail_to_list, data):
        process_message(mail_from, mail_to_list, data)


server = SMTPServer((INCOMING_SMTP_SERVER_HOST, INCOMING_SMTP_SERVER_PORT), None)
try:
    asyncore.loop()
except KeyboardInterrupt:
    pass
